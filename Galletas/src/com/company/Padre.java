package com.company;

public class Padre extends Thread{
    private String nombre;
    private Pote pote;

    public Padre(String nombre, Pote pote) {
        this.nombre = nombre;
        this.pote = pote;
    }

    @Override
    public void run() {
        try {
            while (pote.getGalletas() > 0) {
                wait();
            }
            pote.setGalletas(pote.capacidad);
            notifyAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
