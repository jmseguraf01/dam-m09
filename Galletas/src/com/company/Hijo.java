package com.company;

public class Hijo extends Thread{
    private String nombre;
    private Pote pote;
    private final int galletasComer = 5;

    public Hijo(String nombre, Pote pote) {
        this.nombre = nombre;
        this.pote = pote;
    }

    @Override
    public void run() {
        try {
            pote.coger();
            System.out.println(nombre + " ha cogido el pote con " + pote.getGalletas() + " galletas.");
            pote.comerGalletas(galletasComer);
            System.out.println(nombre + " se ha comido " +galletasComer+ " galletas.");
            pote.soltar();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
