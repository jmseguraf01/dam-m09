package com.company;

public class Main {

    public static void main(String[] args) {
        Pote pote = new Pote();
        // Hijos
        Hijo juanmi = new Hijo("juanmi", pote);
        Hijo laura = new Hijo("laura", pote);
        Hijo gonzalo = new Hijo("gonzalo", pote);
        Hijo luis = new Hijo("luis", pote);
        // Padres
        Padre padre = new Padre("pepe", pote);
        Padre madre = new Padre("luisa", pote);

        juanmi.start();
        laura.start();
        gonzalo.start();
        luis.start();
        padre.start();
        madre.start();
    }
}
