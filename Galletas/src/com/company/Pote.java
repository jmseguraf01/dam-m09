package com.company;

public class Pote {
    private int galletas;
    final int capacidad = 20;
    private boolean libre;

    public Pote() {
        galletas = capacidad;
        libre = true;
    }

    synchronized void coger() throws InterruptedException {
        while (!libre) {
            wait();
        }
        libre = false;
        notifyAll();
    }

    synchronized void soltar() {
        libre = true;
        notifyAll();
    }

    public int getGalletas() {
        return galletas;
    }

    void comerGalletas(int galletas) {
        this.galletas -= galletas;
    }

    public void setGalletas(int galletas) {
        this.galletas = galletas;
    }
}
