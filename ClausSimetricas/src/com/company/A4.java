package com.company;

import javax.crypto.SecretKey;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class A4 {
    public static void main(String[] args) throws IOException {
        ej1_5();
        ej1_6();
        ej1_8();
        ej2();
    }

    // Cifrar un texto con clave generada aleatoria
    private static void ej1_5() {
        int keySize = 128;
        SecretKey secretKey = Utils.keygenKeyGeneration(keySize);
        String text = "Texto encriptado con clave aleatoria.";
        // Encripto el texto que le paso
        byte[] textoEncriptado = Utils.encryptData(secretKey, text.getBytes());
        // Desencripto el texto
        byte[] textoDesencriptado = Utils.decryptData(secretKey, textoEncriptado);
        // Print
        System.out.println("Texto encriptado: " + new String(textoEncriptado));
        System.out.println("Texto desencriptado: " + new String(textoDesencriptado));
        probarMetodos(secretKey);
    }

    // Cifrar un texto con una clave especificada
    private static void ej1_6() {
        int keysize = 128;
        String keyText = "password123";
        SecretKey secretKey = Utils.passwordKeyGeneration(keyText, keysize);
        boolean secreKeyErroneo = false;
        String text = "Texto encriptado con clave especificada";
        // Encripto
        byte[] textoEnriptado = Utils.encryptData(secretKey, text.getBytes());
        // Desencripto
        byte[] textoDesencriptado = Utils.decryptData(secretKey, textoEnriptado);
        System.out.println("Texto encriptado: " + new String(textoEnriptado));
        System.out.println("Texto desencriptado: " + new String(textoDesencriptado));
        probarMetodos(secretKey);
    }

    // Probar metodos Secrey Key
    private static void probarMetodos(SecretKey secretKey) {
        System.out.println("---- Secret Key data ----");
        System.out.println("Algoritmo: " + secretKey.getAlgorithm());
        System.out.println("Formato: " + secretKey.getFormat());
        System.out.println("Hash code: " + secretKey.hashCode());
        System.out.println("----------------");
    }

    // Ejercicio para que falle la desencriptacion
    private static void ej1_8() {
        int keysize = 128;
        String keyText = "password123";
        SecretKey secretKey = Utils.passwordKeyGeneration(keyText, keysize);
        String text = "Texto encriptado con clave especificada";
        // Encripto
        byte[] textoEnriptado = Utils.encryptData(secretKey, text.getBytes());
        // Cambio la secretkey para que me de error
        secretKey = Utils.keygenKeyGeneration(keysize);
        // Desencripto, y me dara error
        byte[] textoDesencriptado = Utils.decryptData(secretKey, textoEnriptado);
    }

    // Desencripta el texto de un fichero con un fichero de donde coge las contraseñas
    private static void ej2() throws IOException {
        // Ficheros
        File fileTextAmagat = new File(System.getProperty("user.home") + "\\Desktop\\mp09\\textamagat");
        File fileClaus = new File(System.getProperty("user.home") + "\\Desktop\\mp09\\clausA4.txt");
        // Obtengo el texto encriptado que hay en el fichero
        Path path = Paths.get(String.valueOf(fileTextAmagat));
        byte[] textEncriptado = Files.readAllBytes(path);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileClaus));
        byte[] textoDesencriptado = null;
        // Siempre que no se haya desencriptado el texto
        while (textoDesencriptado == null) {
            // Obtengo la secretkey
            SecretKey secretKey = Utils.passwordKeyGeneration(bufferedReader.readLine(), 128);
            textoDesencriptado = Utils.decryptData(secretKey, textEncriptado);
        }
        // Imprimo el texto desencriptado
        System.out.println(new String(textoDesencriptado));

    }

}
