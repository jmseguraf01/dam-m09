package com.company;

import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Scanner;

public class A5 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        ej1_1();
        ej1_2();
        ej3();
        ej4();
        ej5();
        ej6();
        ej2_2();
    }

    // Encripto y desencripto un mensaje
    private static void ej1_1() {
        // Creo un par de claves
        KeyPair keyPair = Utils.randomGenerate(1024);
        System.out.print("Escribe el mensaje que deseas encriptar: ");
        String mensaje = scanner.nextLine();
        // Encripto el mensaje con la clave publica
        byte[] mensajeEncriptado = Utils.encryptData(mensaje.getBytes(), keyPair.getPublic());
        System.out.println("Mensaje encriptado: " + new String(mensajeEncriptado));
        // Desencripto el mensaje con la clave privada
        byte[] mensajeDesencriptado = Utils.decryptData(mensajeEncriptado, keyPair.getPrivate());
        System.out.println("Mensaje desencriptado: " + new String(mensajeDesencriptado));

    }

    private static void ej1_2() {
        // Cargo un keystore y saco su informacion
        String ksFile = System.getProperty("user.home") + "/.keystore";
        try {
            KeyStore keyStore = Utils.loadKeyStore(ksFile, "123456");
            System.out.println("Tipus keystore: " + keyStore.getType());
            System.out.println("Mida: " + keyStore.size());
            System.out.print("Alias: [");
            Enumeration enumeration = keyStore.aliases();
            while (enumeration.hasMoreElements()) {
                System.out.print(enumeration.nextElement() + " ");
            }
            System.out.print("]\n");
            // Muestro el certificado y el algoritmo
            System.out.println("Certificado de mykey: " + keyStore.getCertificate("mykey"));
            // Creo una nueva clave simetrica (secretkey) y la guardo en el keystore
            SecretKey secretKey = Utils.keygenKeyGeneration(128);
            // Contraseña para el nuevo keystore
            char[] password = "123456".toCharArray();
            // Formateo la secretkey para poder meterlo en el keystore
            KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(secretKey);
            KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(password);
            // Meto la secretkey en el keystore
            keyStore.setEntry("mykey2", secretKeyEntry, protParam);
            // Guardo el keystore con el mismo nombre que el que he cargado, y pongo la misma contraseña
            // En el caso de poner otro nombre, se crearia otro fichero
            // En caso de poner el mismo nombre y diferente contraseña, se sustituye por la anterior
            keyStore.store(new FileOutputStream(ksFile), "123456".toCharArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Muestra los datos de una publickey por pantalla
    private static void ej3() {
        String fichero = System.getProperty("user.home") + "\\Desktop\\jordi.cer";
        try {
            PublicKey publicKey = Utils.getPublicKey(fichero);
            System.out.println(publicKey);
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println("No se encuentra el fichero");
        }
    }


    // Muestra la publickey de un keystore
    private static void ej4() {
        // Datos del keystore
        String ksFile = System.getProperty("user.home") + "\\Desktop\\.keystore";
        String alias = "mykey";
        String password = "123456";
        try {
            // Cargo el keystore
            KeyStore keyStore = Utils.loadKeyStore(ksFile, password);
            PublicKey publicKey = Utils.getPublicKey(keyStore, alias, password);
            System.out.println(publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Creo un par de claves y obtengo su firma
    private static void ej5() {
        KeyPair keyPair = Utils.randomGenerate(1024);
        byte[] signature = Utils.signData("texto".getBytes(), keyPair.getPrivate());
        System.out.println(new String(signature));
    }

    // Le paso la firma de una llave y me devuelve la validacion
    private static void ej6() {
        // Creo un par de llaves
        KeyPair keyPair = Utils.randomGenerate(1024);
        byte[] texto = "datos".getBytes();
        // Obtengo la firma
        byte[] signature = Utils.signData(texto, keyPair.getPrivate());
        // Valido la firma que me ha devuelto anteriormente
        boolean firmaValida = Utils.validateSignature(texto, signature, keyPair.getPublic());
        System.out.println(firmaValida);

        /*
        * Podemos comprobar que el metodo funciona bien, si creamos otra keypair y le pasamos la publickey de esa keypair
        * Seria el siguiente codigo:
        * KeyPair keyPair2 = Utils.randomGenerate(1024);
        * boolean firmaValida = Utils.validateSignature(texto, signature, keyPair2.getPublic());
        * */
    }


    // Encripto y desencripto un mensaje
    private static void ej2_2() {
        KeyPair keyPair = Utils.randomGenerate(1024);
        byte[] texto = "texto".getBytes();
        byte[][] encWrappedData = Utils.encryptWrappedData(texto, keyPair.getPublic());
        byte[] decryptWrappedData = Utils.decryptWrappedData(encWrappedData, keyPair.getPrivate());
        System.out.println(new String(decryptWrappedData));

    }

}
