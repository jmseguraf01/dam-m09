package com.company;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Scanner;

public class Cliente {
    private DatagramSocket socket;

    public void main() throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        socket = new DatagramSocket();
        // Direccion IP del servidor
        InetAddress ipServidor = InetAddress.getByName("localhost");
        int puertoDestino = 5000;
        Tauler tauler;
        Jugada jugada = new Jugada();
        byte [] receivingData = new byte[2048];
        int multiPort = 5557;
        // Pido el nombre
        System.out.print("Jugador: ");
        jugada.Nom = scanner.nextLine();

        // Bucle de juego
        while (true) {
            System.out.print("Numero: ");
            jugada.num = scanner.nextInt();
            scanner.nextLine();
            // Creo el mensaje a enviar
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(jugada);
            // Creo el paquete
            DatagramPacket packet = new DatagramPacket(os.toByteArray(), os.toByteArray().length, ipServidor, puertoDestino);
            // Envio el mensaje
            socket.send(packet);
            // Espero la respuesta
            packet = new DatagramPacket(receivingData, 2048);
            socket.receive(packet);
            // Leo el tablero
            tauler = processData(packet);
            System.out.println(tauler);
            if (acertado(tauler.resultat)) {
                break;
            }
        }

        MulticastSocket socketMulticast = new MulticastSocket(multiPort);
        socketMulticast.joinGroup(InetAddress.getByName(ServidorAdivinaUDP_Obj.ipMulticast));
        // Bucle en el que cliente espera hasta que acaban los demas
        while (tauler.acabats < tauler.map_jugadors.size()) {
            DatagramPacket packet = new DatagramPacket(receivingData, 2048);
            socketMulticast.receive(packet);
            tauler = processData(packet);
            System.out.println(tauler);
        }
    }

    private Tauler processData(DatagramPacket packet) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(packet.getData());
        ObjectInputStream ois = new ObjectInputStream(in);
        return (Tauler) ois.readObject();
    }

    // Compruebo si el numero es igual, mas grande o mas pequeño
    public boolean acertado(int num) {
        if (num == 0) {
            System.out.println("Correcto");
            return true;
        } else if (num == 1) {
            System.out.println("Mes petit");
            return false;
        } else {
            System.out.println("Mes gran");
            return false;
        }
    }
}
