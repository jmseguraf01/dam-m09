package com.company;

public class Caracol implements Runnable {

    private String nombre;
    private float metros;

    public Caracol(String nombre) {
        this.nombre = nombre;
        this.metros = 0f;
    }

    public String getNombre() {
        return nombre;
    }

    public float getMetros() {
        return metros;
    }

    // Añado X metros al caracol
    @Override
    public void run() {
        metros += Math.floor(Math.random() * 5);
    }
}
