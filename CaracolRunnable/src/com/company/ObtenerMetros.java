package com.company;

public class ObtenerMetros implements Runnable {
    private Caracol caracol;

    public ObtenerMetros(Caracol caracol) {
        this.caracol = caracol;
    }

    public Caracol getCaracol() {
        return caracol;
    }


    // Obtengo los metros del caracol
    @Override
    public void run() {
        System.out.println("El caracol " + caracol.getNombre() + " ha recorrido " + caracol.getMetros() + " metros.");

    }
}
