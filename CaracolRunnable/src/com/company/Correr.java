package com.company;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Correr {

    public static void main(String[] args) throws InterruptedException {
        Caracol[] caracols = new Caracol[2];

        // Clases caracoles
        caracols[0] = new Caracol("caracol pepito");
        caracols[1] = new Caracol("caracol jose");

        // Las clases de obtener los metros
        ObtenerMetros obtenerMetrosCaracol1 = new ObtenerMetros(caracols[0]);
        ObtenerMetros obtenerMetrosCaracol2 = new ObtenerMetros(caracols[1]);

        // Ejecuto el proceso del caracol, que es el de andar X metros
        ScheduledExecutorService schExService = Executors.newScheduledThreadPool(3);
        schExService.scheduleWithFixedDelay(caracols[0], 2, 2, TimeUnit.SECONDS);
        schExService.scheduleWithFixedDelay(caracols[1], 6, 1, TimeUnit.SECONDS);

        // Ejecuto el proceso para saber los metros que ha recorrido cada caracol
        schExService.scheduleWithFixedDelay(obtenerMetrosCaracol1, 5, 3,TimeUnit.SECONDS);
        schExService.scheduleWithFixedDelay(obtenerMetrosCaracol2, 6, 3,TimeUnit.SECONDS);

        // Termino el proceso despues de 25 segundos
        schExService.awaitTermination(25, TimeUnit.SECONDS);
        schExService.shutdown();
    }
}
