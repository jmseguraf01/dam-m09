package com.company;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class Cliente {
    private DatagramSocket socket;

    public void main() throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        socket = new DatagramSocket();
        // Direccion IP del servidor
        InetAddress ipServidor = InetAddress.getByName("localhost");
        int puertoDestino = 5000;
        Tauler tauler;
        Jugada jugada = new Jugada();
        byte [] receivingData = new byte[2048];
        NumeroSecreto numeroSecreto = new NumeroSecreto();

        while (true) {
            System.out.print("Jugador: ");
            jugada.Nom = scanner.nextLine();
            System.out.print("Numero: ");
            jugada.num = scanner.nextInt();
            scanner.nextLine();
            // Creo el mensaje a enviar
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(jugada);
            // Creo el paquete
            DatagramPacket packet = new DatagramPacket(os.toByteArray(), os.toByteArray().length, ipServidor, puertoDestino);
            // Envio el mensaje
            socket.send(packet);
            // Espero la respuesta
            packet = new DatagramPacket(receivingData, 2048);
            socket.receive(packet);
            // Leo el tablero
            ByteArrayInputStream in = new ByteArrayInputStream(packet.getData());
            ObjectInputStream ois = new ObjectInputStream(in);
            tauler = (Tauler) ois.readObject();
            System.out.println(tauler);
            System.out.println(numeroSecreto.comprova(String.valueOf(tauler.resultat)));
        }
    }
}
