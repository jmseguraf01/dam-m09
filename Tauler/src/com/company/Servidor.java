package com.company;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Servidor {
    private int puerto;
    private DatagramSocket socket;

    public Servidor(int puerto) throws SocketException {
        this.puerto = puerto;
        socket = new DatagramSocket(puerto);
    }

    public void main() throws IOException, ClassNotFoundException {
        byte [] receivingData = new byte[2048];
        byte [] sendingData;
        InetAddress clientIP;
        int clientPort;
        Tauler tauler = new Tauler();
        Jugada jugada;
        NumeroSecreto numeroSecreto = new NumeroSecreto(5);

        while(true) {
            DatagramPacket packet = new DatagramPacket(receivingData, 2048);
            socket.receive(packet);
            clientIP = packet.getAddress();
            clientPort = packet.getPort();
            // Leo el jugador que envia el cliente
            ByteArrayInputStream in = new ByteArrayInputStream(packet.getData());
            ObjectInputStream ois = new ObjectInputStream(in);
            jugada = (Jugada) ois.readObject();
            tauler.map_jugadors.put(jugada.Nom, jugada.num);
            // Compruebo el resultado
            tauler.resultat = numeroSecreto.comprova(jugada.num);
            // Creo el tablero con el jugador
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(tauler);
            System.out.println("Jugada: " + jugada);
            System.out.println("Tablero: \n" + tauler);
            // Envio tablero
            packet = new DatagramPacket(os.toByteArray(), os.toByteArray().length, clientIP, clientPort);
            socket.send(packet);
        }
    }

}
