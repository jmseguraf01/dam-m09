package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class GoogleSend {
    final static String nombre = "entry.835030737";
    final static String radioButton = "entry.1616686619";
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        System.out.print("Escribe el nombre: ");
        String nombreData = scanner.nextLine();
        System.out.print("Si o no?: ");
        String radioButtonData = scanner.nextLine();

        System.out.println("Enviando datos...");

        URL url  = new URL("https://docs.google.com/forms/u/0/d/e/1FAIpQLScE6sxLFb3BmCmT2TKHQH5ormS0qvjHwO-uTAR8MXaagBvSSQ/formResponse");
        URLConnection connection = url.openConnection();
        connection.setDoOutput(true);
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(nombre + "=" + nombreData + "&" + radioButton + "=" + radioButtonData);
        out.close();

        // Importante, hay que poner la siguiente linea para recoger la respuesta
        connection.getInputStream();

        System.out.println("Datos enviados");
    }
}
