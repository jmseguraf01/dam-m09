package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {

    public static void main(String[] args) throws IOException {
        URL url  = new URL(args[0]);
        BufferedReader strings = new BufferedReader(new InputStreamReader(url.openStream()));
        String linea;
        while ((linea = strings.readLine()) != null) {
            if (linea.contains(args[1])) {
                System.out.println(linea);
            }
        }
        strings.close();
    }
}
