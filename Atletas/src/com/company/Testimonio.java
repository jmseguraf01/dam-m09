package com.company;

public class Testimonio {
    private boolean libre;
    private Equipo equipo;
    private int tiempo;

    enum Equipo {
        A, B;
    }

    public Testimonio(Equipo equipo) {
        this.equipo = equipo;
        this.libre = true;
        this.tiempo = 0;
    }

    synchronized void coger() throws InterruptedException {
        // Mientras no este libre, se espera
        while (!libre) {
            wait();
        }
        libre = false;
        // Notifico el cambio de la variable
        notifyAll();
    }


    synchronized void soltar() {
        libre = true;
        notifyAll();
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void addTiempo(int tiempo) {
        this.tiempo += tiempo;
    }

    public int getTiempo() {
        return tiempo;
    }
}
