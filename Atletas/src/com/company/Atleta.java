package com.company;

public class Atleta extends Thread{
    private String nombre;
    Testimonio testimonio;

    public Atleta(String nombre, Testimonio testimonio) {
        this.nombre = nombre;
        this.testimonio = testimonio;
    }

    @Override
    public void run() {
        try {
            testimonio.coger();
            System.out.println("El jugador " + nombre + " ha cogido el testimonio del equipo " + testimonio.getEquipo() + ".");
            int tiempo = (int) (Math.random() * 5);
            Thread.sleep(tiempo * 1000);
            testimonio.soltar();
            // Añado el tiempo que ha tardado
            testimonio.addTiempo(tiempo);
            System.out.println("El jugador " + nombre + " ha soltado el testimonio del equipo " + testimonio.getEquipo() + ".");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
