package com.company;

import java.util.concurrent.ForkJoinPool;

public class Main {

    public static void main(String[] args) {
        int procesadores = Runtime.getRuntime().availableProcessors();
        ForkJoinPool pool = new ForkJoinPool(procesadores);

        Factoria factoria = new Factoria(4);

        long resultado = pool.invoke(factoria);

        long result = factoria.join();

        System.out.println(result);

    }
}
