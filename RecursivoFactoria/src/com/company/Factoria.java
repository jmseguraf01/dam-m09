package com.company;

import java.util.concurrent.RecursiveTask;

public class Factoria extends RecursiveTask<Long> {

    private long n;

    public Factoria(long n) {
        this.n = n;
    }

    public Long getResultadoSecuencial() {
        long result = 1;
        for (int x=2; x<= n; x++)
             result = result * x;
        return result;
    }

    public Long getResultadoRecursivo() {
        Factoria factoria = new Factoria(n);
        factoria.fork();
        return factoria.join();
    }

    @Override
    protected Long compute() {
        if (n > 5) {
            return getResultadoSecuencial();
        } else {
            n -= 1;
            return getResultadoRecursivo();
        }
    }
}
