package com.company.udp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;


public class ClientVelocimetre {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        MulticastSocket socketMulticast = new MulticastSocket(SrvVelocitats.multiPort);
        socketMulticast.joinGroup(InetAddress.getByName(SrvVelocitats.ipMulticast));
        byte [] receivingData = new byte[4];
        int x = 0;
        int numero = 0;
        while (true) {
            DatagramPacket packet = new DatagramPacket(receivingData, 4);
            socketMulticast.receive(packet);
            numero += processData(packet);
            x++;
            if (x == 5) {
                System.out.println("La media es: " + numero / 5);
                numero = 0;
                x = 0;
            }
        }
    }

    // Procesa el array de bytes en entero
    private static int processData(DatagramPacket packet) throws IOException, ClassNotFoundException {
        int num = ByteBuffer.wrap(packet.getData()).getInt();
        return num;
    }

}
