package com.company.tcp;

import java.io.*;
import java.net.Socket;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ThreadServidor implements Runnable{
    Socket clientSocket = null;
    ObjectOutputStream oos = null;
    ObjectInputStream ois = null;
    boolean acabat;
    Llista llista;

    public ThreadServidor(Socket clientSocket) throws IOException, ClassNotFoundException {
        this.clientSocket = clientSocket;
        acabat = false;
        oos = new ObjectOutputStream(clientSocket.getOutputStream());
        ois = new ObjectInputStream(clientSocket.getInputStream());
    }



    @Override
    public void run() {
        while(!acabat) {
            try {
                llista = (Llista) ois.readObject();
                oos.writeObject(ordenarLista(llista));
                oos.flush();
                acabat = true;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private Llista ordenarLista(Llista llista) {
        List<Integer> numeros = llista.getNumberList();
        Collections.sort(numeros);
        List<Integer> numerosSinDuplicados = numeros.stream().distinct().collect(Collectors.toList());
        llista.setNumberList(numerosSinDuplicados);
        return llista;
    }
}
