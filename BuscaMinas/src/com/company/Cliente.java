package com.company;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Cliente {
    private InetAddress ipServer;
    private int puertoServidor;
    private Scanner scanner = new Scanner(System.in);

    public Cliente(String ipServer, int puertoServidor) {
        try {
            this.ipServer = InetAddress.getByName(ipServer);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.puertoServidor = puertoServidor;
    }

    // Inicia el cliente
    public void start() throws IOException {
        byte [] receivedData = new byte[1024];
        DatagramPacket packet;
        DatagramSocket socket = new DatagramSocket();
        // Pido el nombre y inicio el tablero
        System.out.print("Escribe tu nombre: ");
        Tablero tablero = new Tablero(scanner.nextLine());

        while (true) {
            realizarTirada(tablero);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(tablero);
            byte[] missatge = os.toByteArray();

            packet = new DatagramPacket(missatge, missatge.length, ipServer, puertoServidor);
            // Envio el mensaje al servidor
            socket.send(packet);
            // Espero la respuesta del servidor
            packet = new DatagramPacket(receivedData, 1024);
            socket.setSoTimeout(5000);
            try {
                // Recibo la respuesta y proceso el tablero
                socket.receive(packet);
                tablero = dataToTablero(packet.getData(), packet.getLength());
                // Ha perdido
                if (tablero.isPartidaPerdida()) {
                    System.out.println(" - - HAS PERDIDO - - ");
                    System.out.println("EN ESA POSICIÓN HAY UNA MINA!!");
                    break;
                } else {
                    System.out.println(tablero);
                }
            } catch(SocketTimeoutException e) {
                System.out.println("El servidor no responde: " + e.getMessage());
            }
        }
    }

    // Realiza la tirada el jugador
    private void realizarTirada(Tablero tablero) {
        System.out.println("- - Tirada - -");
        int fila, columna;
        System.out.println("Las filas y columnas van desde el 0 al 4");
        do {
            System.out.print("Escribe la fila: ");
            fila = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Escribe la columna: ");
            columna = scanner.nextInt();
            scanner.nextLine();
        } while (fila > 4 || columna > 4);
        tablero.setTirada(fila, columna);
    }

    // Procesa el resultado del tablero del servidor
    private Tablero dataToTablero(byte[] data, int length) {
        Tablero tablero = null;
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        try {
            ObjectInputStream ois = new ObjectInputStream(in);
            tablero = (Tablero) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return tablero;
    }
}
