package com.company;

import java.io.IOException;
import java.util.Scanner;

public class Menu {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("------------------------");
        System.out.println("PESCA - MINA");
        System.out.println("------------------------");
        System.out.println("1. Jugar");
        System.out.println("0. Salir");
        System.out.println("------------------------");
        System.out.print("Introduce la opcion a elegir: ");
        int opcion = scanner.nextInt();



        switch (opcion) {
            case 1:
                System.out.print("Ingresa la ip del servidor: ");
                String ipSrv = scanner.nextLine();
                scanner.nextLine();
                System.out.print("Ingresa el puerto a utilizar: ");
                int portSrv = scanner.nextInt();
                new Cliente(ipSrv, portSrv).start();
                break;
            case 0:
                System.out.println("Saliendo del juego...");
                break;

        }
    }


}
