package com.company;

import java.io.Serializable;
import java.util.Arrays;

public class Tablero implements Serializable {
    private final int size = 5;
    private final int numeroMinas = 10;
    private final int totalSize = size * size;
    private String nombre;
    private int tiradaX;
    private int tiradaY;
    private boolean perdido;
    private String tablero[][] = new String[size][size];
    private int tableroConMinas[][] = new int[size][size];
    private boolean partidaPerdida;
    private int[] minas;

    public Tablero(String nombre) {
        this.nombre = nombre;
        perdido = false;
        partidaPerdida = false;
        inicializarTablero();
    }

    public int getTiradaX() {
        return tiradaX;
    }

    public int getTiradaY() {
        return tiradaY;
    }

    public void setTirada(int x, int y) {
        tiradaX = x;
        tiradaY = y;
        comprobarTirada();
        // Siempre que no haya perdido
        if (!partidaPerdida) {
            tablero[x][y] = "X";
        }
    }

    @Override
    public String toString() {
        getTablero();
        return nombre;
    }

    public void getTablero() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(tablero[i][j]);
            }
            System.out.println();
        }
    }

    public String getNombre() {
        return nombre;
    }

    // Comprueba si la tirada toca en una mina
    private void comprobarTirada() {
        if (tableroConMinas[tiradaX][tiradaY] == 5) {
            partidaPerdida = true;
        }
    }

    private void inicializarTablero() {
        // Creamos las posiciones randoms de las minas
        minas = new int[numeroMinas];
        for (int i = 0; i < numeroMinas; i++) {
            minas[i] = (int) (Math.random() * totalSize);
        }

        // Inicializo el tablero
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                tablero[i][j] = "O";
                tableroConMinas[i][j] = 0;
            }
        }

        // Pongo un 5 en las posiciones del tablero con minas
        int x = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                // Reviso si en esta posicion hay una mina
                for (int k = 0; k < minas.length; k++) {
                    if (minas[k] == x) {
                        tableroConMinas[i][j] = 5;
                    }
                }
                x++;
            }
            x++;
        }


        // Muestra el tablero al principio del juego
//        for (int i = 0; i < size; i++) {
//            for (int j = 0; j < size; j++) {
//                System.out.print(tableroConMinas[i][j]);
//            }
//            System.out.println("");
//        }

    }

    public boolean isPartidaPerdida() {
        return partidaPerdida;
    }
}
