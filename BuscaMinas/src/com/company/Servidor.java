package com.company;

import java.io.*;
import java.net.*;

public class Servidor {
    private DatagramSocket socket;
    private boolean continueRunning;
    private int puerto;
    private ObjectOutputStream oos = null;
    private ObjectInputStream ois = null;


    public Servidor(int puerto) {
        this.puerto = puerto;
        continueRunning = true;
        try {
            socket = new DatagramSocket(puerto);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void run() throws IOException {
        byte [] receivingData = new byte[1024];
        byte [] sendingData;
        InetAddress clientIP;
        int clientPort;

        // Atiendo el puerto hasta que no indique lo contrario
        while(continueRunning) {
            // Creo el paquete para recibir los datos
            DatagramPacket packet = new DatagramPacket(receivingData, receivingData.length);
            // Espero el dato
            socket.receive(packet);
            //processament de les dades rebudes i obtenció de la resposta
            sendingData = processData(packet.getData(), packet.getLength());
            // Ip y puerto del cliente
            clientIP = packet.getAddress();
            clientPort = packet.getPort();
            // Creo el paquete para enviar la respuesta
            packet = new DatagramPacket(sendingData, sendingData.length, clientIP, clientPort);
            // Envio la respuesta
            socket.send(packet);
        }
        socket.close();
    }

    // Metodo que obtiene el byte array, lo convierte a objeto y lo devuelve en byte array
    private byte[] processData(byte[] data, int length) {
        Tablero tablero = null;
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        try {
            ObjectInputStream ois = new ObjectInputStream(in);
            tablero = (Tablero) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Tablero de " + tablero.getNombre() + " recibido.");

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(os);
            oos.writeObject(tablero);
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] resposta = os.toByteArray();
        return resposta;
    }

    public static void main(String[] args) throws IOException {
        new Servidor(1355).run();
    }

}
